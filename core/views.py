from django.views import View
from .models import Restaurant
from .api import ApiCaller


class Page(View):
    context = {}
    template = ""
    view_name = None
    app_name = None
    slideshow_images = None
    restaurant = None
    api = None

    # Collects necessary information to build the page. Should be run at the start of every View.
    def prepare(self, request, slideshowimage_class):
        self.app_name, unused, self.view_name = request.resolver_match._func_path.split('.')
        self.restaurant = Restaurant.objects.get(app_name=self.app_name)
        self.api = ApiCaller(self.restaurant)

        images = slideshowimage_class.objects.filter(page_class=self.view_name)
        if self.restaurant.ribbon:
            ribbon = self.restaurant.ribbon
        else:
            ribbon = self.restaurant.logo

        self.context['slideshow_images'] = images
        self.context['slideshow_aspect_ratio'] = 0
        self.context['logo'] = self.restaurant.logo
        self.context['ribbon'] = ribbon

        for image in images:
            aspect_ratio = image.image.width / image.image.height
            if aspect_ratio > self.context['slideshow_aspect_ratio']:
                self.context['slideshow_aspect_ratio'] = aspect_ratio

    # Removes empty categories based on whether it has a price for a type of service
    @staticmethod
    def filter_unpriced_categories(api_response, menu_item_property_name):
        menu = []
        for category in api_response['categories']:
            for item in category['menu_items']:
                if item[menu_item_property_name]:
                    menu.append(category)
                    break

        return menu
