import json

from django.test import TestCase
from .api import ApiCaller
from django.conf import settings
from .models import Restaurant
import requests


class ApiTests(TestCase):
    def test_if_can_connect_to_test_endpoint(self):
        response = requests.get(url=settings.API_ADDRESS + 'test/')
        self.assertNotEqual(response.status_code, 404, msg="Test endpoint not found")

    def test_if_restaurants_can_access_menu(self):
        restaurant = Restaurant(
            id=3,
            name='TESTaurant',
            app_name='test',
            static_folder_name='test',
            logo='',
            api_key='6pgXeiPC.i5mNsAnfZJX0S12OUkzvxBiMMCgLJK0M',
        )
        api = ApiCaller(restaurant)
        menu_ids = api.get_request('menu/ids').content.decode("UTF-8")
        self.assertGreater(json.loads(menu_ids), 0, msg="No menu found for the test restaurant")
