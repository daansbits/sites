from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Restaurant, User


class RestaurantAdmin(admin.ModelAdmin):
    exclude = ()
    ordering = ['name']


class CmsUserAdmin(UserAdmin):
    pass


admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(User, CmsUserAdmin)
