from django.conf import settings
import requests


class ApiCaller:
    restaurant = None
    api_key = None

    def __init__(self, restaurant):
        self.restaurant = restaurant
        self.api_key = self.restaurant.api_key

    # Make a GET request to the API
    def get_request(self, url):
        get_url = settings.API_ADDRESS + 'restaurant/{0}/{1}/'.format(self.restaurant.id, url)
        response = requests.get(
            url=get_url,
            headers={
                'Authorization': 'Api-Key ' + self.restaurant.api_key,
                'user-agent': 'site for restaurant {0}'.format(self.restaurant.pk),
            },
        )
        return response

    # Get a specific menu; may need a server side check to see if this restaurant owns this menu
    def get_menu(self, menu_id):
        return self.get_request('menu/' + str(menu_id))

    # Get a list of which menu ids for this restaurant
    def get_menu_ids(self):
        return self.get_request('menu/ids/')
