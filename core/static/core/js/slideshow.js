const slideDuration = 6000;

let slideshow = document.querySelector('#slideshow');
let slides = document.querySelectorAll('#slideshow > .container > img')
let activeIndex = 0;

setInterval(next_slide, slideDuration);

let aspectRatio = slideshow.getAttribute('data-aspectratio')
slideshow.removeAttribute('data-aspectratio');

resize_div()
window.onresize = resize_div;

function resize_div() {
    let newHeight = slideshow.clientWidth / aspectRatio;
    slideshow.setAttribute('style', 'height:' + Math.ceil(newHeight) + "px");
}

function next_slide() {
    if (++activeIndex === slides.length)
        activeIndex = 0;

    if (activeIndex === 0)
        hide(slides.length-1);
    else
        hide(activeIndex-1);

    show(activeIndex)
}

function hide(index) {
    slides[index].style.opacity = "0";
}

function show(index) {
    slides[index].style.opacity = "1";
}