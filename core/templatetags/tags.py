from django import template

register = template.Library()


@register.filter
def to_euro(number):
    return "€ {0:.2f}".format(number/100)
