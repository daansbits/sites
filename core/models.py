from django.contrib.auth.models import AbstractUser
from django.db import models


# Will receive the path of the upload folder for the focused app
def get_upload_folder(instance, filename):
    app_label = instance._meta.app_label
    if instance._meta.model_name == 'restaurant':
        app_label = instance.app_name
    return '{0}/{1}/{2}'.format(app_label, instance._meta.model_name, filename)


class Restaurant(models.Model):
    name = models.CharField(max_length=100)
    app_name = models.CharField(max_length=100)
    static_folder_name = models.CharField(max_length=100)
    logo = models.ImageField(upload_to=get_upload_folder)
    ribbon = models.ImageField(upload_to=get_upload_folder, blank=True, null=True)
    api_key = models.CharField(max_length=50)

    def __str__(self):
        return self.name


def restaurant_news_folder(instance, filename):
    import time
    return "{0}/news/{1}_{2}".format(instance.user.restaurant.app_name, int(time.time()), filename)


class AbsNewsImage(models.Model):
    image = models.ImageField()
    blurb = models.TextField()

    class Meta:
        abstract = True


class AbsNews(models.Model):
    title = models.CharField(max_length=500)
    datetime = models.DateTimeField(auto_now_add=True)
    body = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name_plural = "News Items"

    def __str__(self):
        return self.title


class AbsSlideshowImage(models.Model):
    page_class = models.CharField(max_length=100)
    image = models.ImageField(upload_to=get_upload_folder)

    class Meta:
        abstract = True


class User(AbstractUser):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, null=True)
