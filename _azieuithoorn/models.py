from django.db import models
from core.models import AbsNews, AbsNewsImage, AbsSlideshowImage


class NewsImage(AbsNewsImage):
    pass


class News(AbsNews):
    images = models.ManyToManyField(NewsImage, blank=True)


class SlideshowImage(AbsSlideshowImage):
    pass
