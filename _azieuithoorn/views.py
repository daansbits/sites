from django.shortcuts import render
from core.views import Page
from .models import SlideshowImage, News
import json


class IndexView(Page):
    def get(self, request):
        super(IndexView, self).prepare(request, SlideshowImage)
        self.context['news'] = News.objects.all()
        return render(request, 'azieuithoorn/index.html', self.context)


class TakeoutView(Page):
    def get(self, request):
        super(TakeoutView, self).prepare(request, SlideshowImage)
        api_results = json.loads(self.api.get_menu(1).read().decode("UTF-8"))[0]
        self.context['api'] = self.filter_unpriced_categories(api_results, 'takeout_price')

        return render(request, 'azieuithoorn/takeout.html', self.context)


class TableView(Page):
    def get(self, request):
        super(TableView, self).prepare(request, SlideshowImage)

        response = self.api.get_menu(1)

        if response.status_code is not 200:
            print(response.status_code)
            self.context['api'] = None
            return render(request, 'azieuithoorn/table.html', self.context)

        api_results = json.loads(response.content.decode("UTF-8"))[0]
        self.context['api'] = self.filter_unpriced_categories(api_results, 'table_price')

        return render(request, 'azieuithoorn/table.html', self.context)


class ContactView(Page):
    def get(self, request):
        super(ContactView, self).prepare(request, SlideshowImage)

        return render(request, 'azieuithoorn/contact.html', self.context)
