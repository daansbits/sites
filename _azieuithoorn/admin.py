from django.contrib import admin
from .models import News, NewsImage, SlideshowImage


class NewsAdmin(admin.ModelAdmin):
    exclude = ()
    ordering = ['title']


class NewsImageAdmin(admin.ModelAdmin):
    exclude = ()
    ordering = ['blurb']


class SlideshowImageAdmin(admin.ModelAdmin):
    exclude = ()


admin.site.register(News, NewsAdmin)
admin.site.register(NewsImage, NewsImageAdmin)
admin.site.register(SlideshowImage, SlideshowImageAdmin)
