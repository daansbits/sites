from django.urls import path
from .views import *

urlpatterns = [
    path('', IndexView.as_view(), name='news'),
    path('takeout', TakeoutView.as_view(), name='takeaway'),
    path('table', TableView.as_view(), name='table'),
    path('contact', ContactView.as_view(), name='contact'),
]
